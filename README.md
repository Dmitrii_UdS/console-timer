## Desktop stopwatch app
Three timers - Basic, Good, Happiness. 

7 8 9 - clear (shift clears too)  
4 5 6 - add a shift (shifts are stacking)  
1 2 3 - start/stop Basic/Good/Happiness timer  

Timers are implemented with System.Diagnostics.Stopwatch, so if the timer is started, it will continue counting even when computer is in sleep mode.
___
RU:  
Приложение с тремя таймерами: Basic, Good, Happiness.  

7 8 9 - Обнулить(сдвиг тоже обнуляется)  
4 5 6 - Добавить сдвиг (сдвиги складываются между собой)  
1 2 3 - Включить/выключить первый/второй/третий  

Таймеры реализованы на System.Diagnostics.Stopwatch.  
Если таймер включён, а компьютер ушёл в спящий режим, отсчёт всё равно продолжается.  
